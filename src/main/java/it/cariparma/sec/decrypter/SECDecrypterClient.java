package it.cariparma.sec.decrypter;

import it.cariparma.sec.decrypter.exception.ChiperException;
import it.cariparma.sec.decrypter.exception.IllegalParameterException;
import it.cariparma.sec.decrypter.exception.RequestDecryptionException;
import it.cariparma.sec.decrypter.exception.RequestExpiredException;
import it.cariparma.sec.decrypter.exception.RequestParserException;
import it.cariparma.sec.decrypter.exception.SecurityPropertiesException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.URLDecoder;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * This class provides the functionality of a cryptographic cipher for decryption. In according with the Java Cryptographic Extension (JCE) framework. 
 * In order to create a SECDecrypterClient object, the application calls the SECDecrypterClient's getInstance method. * 
 * 
 * <ul>
 * <li> Snippet 1 - read secret key from properties :
 *  <pre>
 *	....
 *		SECDecrypterClient decrypter = SECDecrypterClient.getInstance();
 *		Map<String, String>  decryptMap = decrypter.decrypt(data, vector);
 *		decryptMap.get(key);
 *	....
 * </pre>
 * 
 * <li> Snippet 2 - passing secret key explicitly:
 *  <pre>
 *	....
 *		SECDecrypterClient decrypter = SECDecrypterClient.getInstance();
 *		Map<String, String>  decryptMap = decrypter.decrypt(key, data, vector);
 * 		decryptMap.get(key);
 *	....
 * </pre>
 * </ul>
 * @author Domenico Tripaldi
 *
 */
public final class SECDecrypterClient {
	private static String TS = "TS";
	private static final String ENCODING = "UTF-8";
	private static final String TRANSFORMATION = "AES/CBC/PKCS5Padding";
	private static final long MINUTE_MILLIS = 60000L;

	/**
	 * Returns a Factory Method SECDecrypterClient object that encapsulating the functionality of a cryptographic cipher for decryption.
	 * @return a client that implements the decryption functionality.
	 */
	public static final SECDecrypterClient getInstance() {
		return new SECDecrypterClient();
	}

	/**
	 * Decrypt the specified data using the embedded cryptographic
	 * @param data  the encrypted data
	 * @param vector the initialization vector (IV)
	 * @return an Map  {@link java.util.Map} containing the decrypted pairs key, value
	 * @throws RequestDecryptionException
	 * @throws RequestExpiredException
	 * @throws ChiperException
	 * @throws IllegalParameterException
	 * @see java.util.Map
	 */
	public final Map<String, String> decrypt(String data, String vector) throws RequestDecryptionException, RequestExpiredException, ChiperException, IllegalParameterException {
		String key = getKey();
		return decrypt( key, data, vector);
	}

	
	/**
	 * Decrypt the specified data using the explicit key
	 * @param key the decryption key
	 * @param data  the encrypted data
	 * @param vector the initialization vector (IV)
	 * @return an Map  {@link java.util.Map} containing the decrypted pairs key, value
	 * @throws RequestDecryptionException
	 * @throws RequestExpiredException
	 * @throws ChiperException
	 * @throws IllegalParameterException
	 * @see java.util.Map
	 */
	public final Map<String, String> decrypt(String key, String data, String vector) throws RequestDecryptionException, RequestExpiredException, ChiperException, IllegalParameterException {

		Object obj = null;
		Map<String, String> decriptedPairs = new HashMap<String, String>();
		try {
			if(key == null){
				throw new RequestDecryptionException("The decryption key is not valid");
			}
			
			String urlDecodedData = URLDecoder.decode(data, ENCODING);
			byte[] encryptedData = Base64.decodeBase64(urlDecodedData.getBytes());

			String urlDecodedIV = URLDecoder.decode(vector, ENCODING);
			byte[] rawIV = Base64.decodeBase64(urlDecodedIV.getBytes());

			Cipher cipher = setupCipher(key, rawIV);

			byte[] decrypted = cipher.doFinal(encryptedData);

			ByteArrayInputStream stream = new ByteArrayInputStream(decrypted);
			ObjectInput in = new ObjectInputStream(stream);
			try{
				obj = in.readObject();
			}
			catch (ClassNotFoundException e){
				throw new RequestDecryptionException("Error during decrypting data - " + e.getCause(), e);
			}
			finally{
				stream.close();
				in.close();
			}
		}
		catch (Exception e){
			throw new RequestDecryptionException("Error during decrypting data - " + e.getCause(), e);
		}
		if (obj != null){
			if ((obj instanceof Map)){
				decriptedPairs = (Map)obj;
			}
			else if ((obj instanceof String)){
				String[] splitData = ((String)obj).split("&");
				for (int i = 0; i < splitData.length; i++) {
					String[] keyValue = splitData[i].split("=");
					decriptedPairs.put(keyValue[0], keyValue[1]);
				}
			}
			else{
				throw new RequestParserException("Error during parsing data string, the object parsed. Data must have the following pattern <key_1>=<value_1>&<key_2>=<value_2>...&<key_N>=<value_N>");
			}
			if (decriptedPairs.isEmpty()) {
				throw new RequestDecryptionException("Error during decrypting data, the object decrypted is empty.");
			}
			try{
				String ts = getIgnoreCaseTimestamp(decriptedPairs);
				Long timestamp = Long.valueOf(Long.parseLong(ts));
				if (timestamp == null) {
					throw new IllegalParameterException("Invalid parameter name or value.");
				}
				if (!isValid(timestamp.longValue())) {
					throw new RequestExpiredException("Invalid request is expired.");
				}
			}
			catch (NumberFormatException nfe){
				throw new RequestDecryptionException("Error during decrypting data - " + nfe.getCause(), nfe);
			}
		}
		else{
			throw new RequestDecryptionException("Error during decrypting data, the object decrypted is null.");
		}
		return decriptedPairs;
	}

	private String getIgnoreCaseTimestamp(Map<String, String> decriptedPairs){
		String upperTSKey = (String)decriptedPairs.get(TS);
		String lowerTSKey = (String)decriptedPairs.get(TS.toLowerCase());
		if ((upperTSKey == null) && (lowerTSKey == null)) {
			throw new IllegalParameterException("Invalid parameter name or value.");
		}
		return upperTSKey != null ? upperTSKey : lowerTSKey;
	}

	private Cipher setupCipher(String decrypterKey, byte[] rawIV) throws ChiperException {
		Cipher cipher = null;
		try {
			cipher = Cipher.getInstance(TRANSFORMATION);
			IvParameterSpec ivSpec = new IvParameterSpec(rawIV);
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] keyByte = decrypterKey.getBytes();
			digest.update(keyByte);
			byte[] key = new byte[16];
			System.arraycopy(digest.digest(), 0, key, 0, key.length);
			SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
			cipher.init(2, keySpec, ivSpec);
		}
		catch (NoSuchAlgorithmException e) {
			throw new ChiperException("The cryptographic algorithm is requested but is not available in the environment - " + e.getCause(), e);
		}
		catch (InvalidKeyException e){
			throw new ChiperException("Invalid Keys (invalid encoding, wrong length, uninitialized, etc) - " + e.getCause(), e);
		}
		catch (InvalidAlgorithmParameterException e) {
			throw new ChiperException("Invalid or inappropriate algorithm parameters - " + e.getCause(), e);
		}
		catch (NoSuchPaddingException e) {
			throw new ChiperException("Padding mechanism is requested but is not available in the environment - " + e.getCause(), e);
		}
		catch (SecurityPropertiesException e) {
			throw new ChiperException(e.getMessage());
		}
		return cipher;
	}

	private boolean isValid(long ts) {
		long now = new Date().getTime();
		return (ts >= now - MINUTE_MILLIS) && (ts <= now + MINUTE_MILLIS);
	}

	private String getKey() throws SecurityPropertiesException {
		String key = null;
		Properties secProperties = new Properties();
		try {
			InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("sec.properties");
			secProperties.load(is);
			is.close();
			key = secProperties.getProperty("secKey");
			if (key == null) {
				throw new SecurityPropertiesException("Missing the propery secKey - ");
			}
		}catch (Exception e) {
			throw new SecurityPropertiesException("Missing the sec.properties - " + e.getMessage(), e);
		}
		return key;
	}
}
