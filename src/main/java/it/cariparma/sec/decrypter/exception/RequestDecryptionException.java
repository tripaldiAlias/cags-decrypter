package it.cariparma.sec.decrypter.exception;

/**
 * Exception thrown when an error occurs during decryption operations.
 * 
 * @author Domenico Tripaldi
 *
 */
public class RequestDecryptionException extends RuntimeException {
	
	private static final long serialVersionUID = -6198447053484362330L;

	/**
	 * Constructor for RequestDecryptionException
	 * 
	 * @param msg the detail message
	 */
	public RequestDecryptionException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for RequestDecryptionException
	 * 
	 * @param msg the detail message
	 * @param cause the root cause from the data access API in use
	 */
	public RequestDecryptionException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
