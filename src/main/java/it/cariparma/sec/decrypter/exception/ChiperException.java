package it.cariparma.sec.decrypter.exception;

/**
 * Exception thrown when an error occurs during configuring the chiper.
 * 
 * @author Domenico Tripaldi
 *
 */
public class ChiperException extends RuntimeException {

	private static final long serialVersionUID = 2570041704985890L;

	/**
	 * Constructor for ChiperException
	 * 
	 * @param msg the detail message
	 */
	public ChiperException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for ChiperException
	 * 
	 * @param msg the detail message
	 * @param cause the root cause from the data access API in use
	 */
	public ChiperException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
