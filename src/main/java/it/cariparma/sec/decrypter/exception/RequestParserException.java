package it.cariparma.sec.decrypter.exception;

/**
 * Exception thrown during data parsing.
 * 
 * @author Domenico Tripaldi
 *
 */
public class RequestParserException extends RuntimeException {

	private static final long serialVersionUID = -5143377159966519465L;

	/**
	 * Constructor for RequestParserException
	 * 
	 * @param msg  the detail message
	 */
	public RequestParserException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for RequestParserException
	 * 
	 * @param msg  the detail message
	 * @param cause the root cause from the data access API in use
	 */
	public RequestParserException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
