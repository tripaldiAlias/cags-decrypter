package it.cariparma.sec.decrypter.exception;

/**
 * This exception is thrown when an invalid parameter or value is passed to a method.
 * 
 * @author Domenico Tripaldi
 *
 */
public class IllegalParameterException extends RuntimeException { 
	
	private static final long serialVersionUID = 3158125164456302119L;

	/**
	 * Constructor for IllegalParameterException
	 * 
	 * @param msg the detail message
	 */
	public IllegalParameterException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for IllegalParameterException
	 * 
	 * @param msg the detail message
	 * @param cause the root cause from the data access API in use
	 */
	public IllegalParameterException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
