package it.cariparma.sec.decrypter.exception;

/**
 * Exception thrown when the request is out of the maximum time
 * 
 * @author Domenico Tripaldi
 *
 */
public class RequestExpiredException extends RuntimeException {

	private static final long serialVersionUID = -3727327093177703376L;

	/**
	 * Constructor for RequestExpiredException
	 * 
	 * @param msg  the detail message
	 */
	public RequestExpiredException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for RequestExpiredException
	 * 
	 * @param msg  the detail message
	 * @param cause the root cause from the data access API in use
	 */
	public RequestExpiredException(String msg, Throwable cause){
		super(msg, cause);
	}
}
