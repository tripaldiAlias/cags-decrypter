package it.cariparma.sec.decrypter.exception;

/**
 * Exception thrown when missing the properties file or the required property
 * 
 * @author Domenico Tripaldi
 *
 */
public class SecurityPropertiesException extends RuntimeException {

	private static final long serialVersionUID = -4877740162183300067L;

	/**
	 * Constructor for SecurityPropertiesException
	 * 
	 * @param msg  the detail message
	 */
	public SecurityPropertiesException(String msg) {
		super(msg);
	}

	/**
	 * Constructor for SecurityPropertiesException
	 * 
	 * @param msg  the detail message
	 * @param cause the root cause from the data access API in use
	 */
	public SecurityPropertiesException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
